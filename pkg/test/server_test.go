package test_test

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"testing"
	"time"

	"gitlab.com/colourdelete/convfrontend/pkg/backend"
	"gitlab.com/colourdelete/convfrontend/pkg/test"
)

func TestServer_Run(t *testing.T) {
	s := test.Server{
		Addr: []string{":8080"},
	}
	errChan := make(chan error)
	s.GoRun(errChan)
	base := backend.NewBase(
		&url.URL{
			Scheme:  "http",
			Host:    "localhost:808",
			Path:    "/",
			RawPath: "/",
		},
		http.Client{Timeout: 5 * time.Second},
		os.Getenv("CONVFRONTEND_PASSWORD"),
	)
	fmt.Println(base)
	err := <-errChan
	if err != nil {
		t.Errorf("server error: %s", err)
	}
	return
}
