package types

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

var ScenarioRefURL *url.URL

func init() {
	var err error
	ScenarioRefURL, err = url.Parse("/demo/conversation/scenario/")
	if err != nil {
		panic(err)
	}
}

// Scenario

func ScenarioURL(baseURL *url.URL) *url.URL {
	return baseURL.ResolveReference(ScenarioRefURL)
}

func ScenarioSingle(baseURL *url.URL, client http.Client, password string, scenarioID int) (ScenarioSingleResp, error) {
	req := ScenarioReq{
		ScenarioID: scenarioID,
		Password:   password,
	}
	reqB, err := json.Marshal(req)
	if err != nil {
		return ScenarioSingleResp{}, err
	}
	reqBuf := bytes.NewBuffer(reqB)
	resp, err := client.Post(
		ScenarioURL(baseURL).String(),
		"application/json",
		reqBuf,
	)
	if err != nil {
		return ScenarioSingleResp{}, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ScenarioSingleResp{}, err
	}
	marshalled := ScenarioSingleResp{}
	err = json.Unmarshal(b, &marshalled)
	if err != nil {
		return ScenarioSingleResp{}, err
	}
	return marshalled, nil
}

func ScenarioMultiple(baseURL *url.URL, client http.Client, password string) (ScenarioMultipleResp, error) {
	req := ScenarioReq{
		ScenarioID: -1,
		Password:   password,
	}
	reqB, err := json.Marshal(req)
	if err != nil {
		return ScenarioMultipleResp{}, err
	}
	reqBuf := bytes.NewBuffer(reqB)
	resp, err := client.Post(
		ScenarioURL(baseURL).String(),
		"application/json",
		reqBuf,
	)
	if err != nil {
		return ScenarioMultipleResp{}, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ScenarioMultipleResp{}, err
	}
	marshalled := ScenarioMultipleResp{}
	err = json.Unmarshal(b, &marshalled)
	if err != nil {
		return ScenarioMultipleResp{}, err
	}
	return marshalled, nil
}

type ScenarioReq struct {
	ScenarioID int    `json:"scenario_id"`
	Password   string `json:"password"`
}

type Scenario struct {
	ID               int    `json:"id"`
	Title            string `json:"title"`
	InitialPrompt    string `json:"initial_prompt"`
	AiName           string `json:"ai_name"`
	HumanName        string `json:"human_name"`
	SummarizeToken   int    `json:"summarize_token"`
	Info             string `json:"info"`
	Description      string `json:"description"`
	ResponseLength   int    `json:"response_length"`
	Temperature      string `json:"temperature"`
	TopP             string `json:"top_p"`
	FrequencyPenalty string `json:"frequency_penalty"`
	PresencePenalty  string `json:"presence_penalty"`
	Duration         int    `json:"duration"`
	Level            int    `json:"level"`
}

type ScenarioSingleResp struct {
	Scenario Scenario `json:"scenario"`
}

type ScenarioMultipleResp struct {
	Scenarios []Scenario `json:"scenarios"`
}
