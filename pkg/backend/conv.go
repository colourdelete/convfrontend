package backend

import (
	"errors"
	"gitlab.com/colourdelete/convfrontend/pkg/types"
	"net/http"
	"net/url"
	"unicode/utf8"
)

type Conv struct {
	baseURL        *url.URL
	client         http.Client
	password       string
	conversationID int
	scenarioData   types.Scenario
}

func (c *Conv) ConversationID() int {
	return c.conversationID
}

func (c *Conv) Scenario() types.Scenario {
	return c.scenarioData
}

func NewConv(baseURL *url.URL, client http.Client, password string, scenarioID int) (*Conv, error) {
	resp, err := types.Start(baseURL, client, scenarioID, password)
	if err != nil {
		return nil, err
	}
	return &Conv{
		baseURL:        baseURL,
		client:         client,
		password:       password,
		conversationID: resp.ConversationID,
		scenarioData:   resp.ScenarioData,
	}, nil
}

// *Conv.Send sends a message with userInput.
func (c *Conv) Send(userInput string) error {
	if !utf8.ValidString(userInput) {
		return errors.New("userInput not valid UTF-8 sequence")
	}
	_, err := types.Chat(c.baseURL, c.client, c.password, c.conversationID, userInput)
	if err != nil {
		return err
	}
	return nil
}

func (c *Conv) Log() ([]types.LogItem, error) {
	return types.LogView(c.baseURL, c.client, c.password, c.conversationID)
}

func (c *Conv) SetLogItem(idx int, item types.LogItem) error {
	panic("implement me")
}
