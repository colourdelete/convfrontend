package backend

import (
	"errors"
	"net/http"
	"net/url"
	"time"

	"fyne.io/fyne/v2"
	"gitlab.com/colourdelete/convfrontend/pkg/types"
)

type Base struct {
	baseURL  *url.URL
	client   http.Client
	password string
}

func NewBase(baseURL *url.URL, client http.Client, password string) Base {
	return Base{
		baseURL:  baseURL,
		client:   client,
		password: password,
	}
}

func NewBaseFromApp(app fyne.App) (Base, error) {
	baseURL, err := url.Parse(app.Preferences().StringWithFallback(
		"session-api-base-url",
		"https://convbackend.herokuapp.com",
	))
	if err != nil {
		return Base{}, err
	}

	clientTimeout, err := time.ParseDuration(app.Preferences().StringWithFallback(
		"session-api-client-timeout",
		"5s",
	))
	if err != nil {
		return Base{}, err
	}
	client := http.Client{
		Timeout: clientTimeout,
	}

	password := app.Preferences().StringWithFallback(
		"session-api-password",
		"",
	)
	if password == "" {
		return Base{}, errors.New("no password set")
	}
	return Base{
		baseURL:  baseURL,
		client:   client,
		password: password,
	}, nil
}

func (b Base) Scenarios() ([]types.Scenario, error) {
	scenarioResp, err := types.ScenarioMultiple(b.baseURL, b.client, b.password)
	if err != nil {
		return nil, err
	}
	return scenarioResp.Scenarios, nil
}

func (b Base) Scenario(scenarioID int) (types.Scenario, error) {
	scenarioResp, err := types.ScenarioSingle(b.baseURL, b.client, b.password, scenarioID)
	if err != nil {
		return types.Scenario{}, err
	}
	return scenarioResp.Scenario, nil
}

func (b Base) Conv(scenarioID int) (*Conv, error) {
	return NewConv(b.baseURL, b.client, b.password, scenarioID)
}
